public class JavaDataType1 {
     public static void main(String[] args) {
        int myNum = 5;               // Integer (whole number)
        float myFloatNum = 5.99f;    // Floating point number
        char myLetter = 'D';         // Character
        boolean myBool = true;       // Boolean
        String myText = "Hello";     // String 
        System.out.println("int : "+myNum);
        System.out.println("float : "+myFloatNum);
        System.out.println("char : "+myLetter);
        System.out.println("boolean : "+myBool);
        System.out.println("String : "+myText);
    }
}
