import java.util.Scanner;

public class YourGrade{
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Please Input Your Score : ");
        int score = scan.nextInt();
        if (score >= 80 && score <= 100) {
            System.out.println("Grade: A");
        }else if(score <= 79 && score >= 75){
            System.out.println("Grade: B+");
        }else if(score <= 74 && score >= 70){
            System.out.println("Grade: B");
        }else if(score <= 69 && score >= 65){
            System.out.println("Grade: C+");
        }else if(score <= 64 && score >= 60){
            System.out.println("Grade: C");
        }else if(score <= 59 && score >= 55){
            System.out.println("Grade: D+");
        }else if(score <= 54 && score >= 50){
            System.out.println("Grade: D");
        }else if(score < 50 ){
            System.out.println("Grade: F");
        }else{
            System.out.println("Failed Please input your score 0 - 100 ");
        }
    }
}